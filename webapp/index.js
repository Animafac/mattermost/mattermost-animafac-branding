/*eslint no-underscore-dangle: ["error", { "allow": ["_paq"] }]*/

class PluginClass {
    initialize(registry) {

    }
}

if (typeof global.window !== 'object') {
    throw new Error('Not in a browser');
}

global.window.registerPlugin('mattermost-animafac-branding', new PluginClass());

var matomo = document.createElement('img');
matomo.style.border = 0;
matomo.src = 'https://analytics.animafac.net/piwik.php?idsite=13&rec=1';
matomo.alt = '';
document.head.appendChild(matomo);
