.PHONY: build test run clean stop check-style

check-style: .npminstall
	@echo Checking for style guide compliance

	cd webapp && yarn run check

test: .npminstall
	@echo Not yet implemented

.npminstall:
	@echo Getting dependencies using npm

	cd webapp && yarn install

build: .npminstall
	@echo Building plugin

	# Clean old dist
	rm -rf dist
	rm -rf webapp/dist
	cd webapp && yarn run build

	# Copy files from webapp
	mkdir -p dist/mattermost-animafac-branding/webapp
	cp webapp/mattermost-animafac-branding_bundle.js dist/mattermost-animafac-branding/webapp/

	# Copy plugin files
	cp plugin.json dist/mattermost-animafac-branding/

	# Compress
	cd dist && tar -zcvf mattermost-animafac-branding.tar.gz mattermost-animafac-branding/*

	# Clean up temp files
	rm -rf dist/mattermost-animafac-branding

	@echo Plugin built at: dist/mattermost-animafac-branding.tar.gz

run: .npminstall
	@echo Not yet implemented

stop:
	@echo Not yet implemented

clean:
	@echo Cleaning plugin

	rm -rf dist
	cd webapp && rm -rf node_modules
	cd webapp && rm -f .npminstall
